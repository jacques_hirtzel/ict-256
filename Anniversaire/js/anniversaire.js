$(function(){

    // détecte le clic sur l'élément "p" qui est le dernier de la div "content" à avoir la classe "open"
    $("#content").on("click","p[class=\"open\"]:last", function(){

        // attribue la classe "open" au premier élément "p" de la div "content" qui ne l'a pas
        $("p:not('.open'):first").addClass("open");

    });

    // Si on clic sur le dernier "p" de la div "content" on lance l'animation de fin et la musique
    $("#content").on("click","p:last", function(){

        // Change les textes des "P"
        $("p").html("Joyeux anniversaire !!!");

        // Affiche les ballons
        $(".ballon").css("display","block");

        // Change le fond
        $("body").css("background-color","rgb(240, 125, 125)");

        // Lancement de la musique
        music.play();

    });

    // Préchargement de la musique
    var music = new Audio("./mp3/JA.mp3");
    music.load();
});