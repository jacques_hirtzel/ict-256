$(function(){

    $("[name='system']").click(function(){
        if($("[name='system']:checked").val() == "dual"){
              $("[value='MPT3']").prop("checked",false);
              $("[value='MPT3']").attr("disabled",true);
        }else{
            console.log("test");
            $("[value='MPT3']").attr("disabled",false);
        }
    });

    $("#inscription_form").validate(
        {
            rules:{
                nom_per:{
                    required : true,
                    nimlength: 2
                },
                prenom_per:{
                    required : true,
                    nimlength: 2
                },
                filiere:{
                    required : true
                },
                system:{
                    required : true
                }
            },
            messages:{
                nom_per:{
                    required : "Veuillez saisir votre nom",
                    nimlength: "Votre nom doit comporter 2 caratères au minimum"
                },
                prenom_per:{
                    required : "Veuillez saisir votre prénom",
                    nimlength: "Votre prénom doit comporter 2 caratères au minimum"
                },
                system:{
                    required :
                        function(){
                            $("#lbl_system").css("color", "red");
                            $(".error_system").remove();
                            $("#lbl_system").append("<span class='error_system'><br>Merci de choisir un systeme</span>");
                        }
                },
                filiere:{
                    required :
                        function(){
                            $("#lbl_filiere").css("color", "red");
                            $(".error_filiere").remove();
                            $("#lbl_filiere").append("<span class='error_filiere'><br>Merci de choisir une filière");
                        }
                }
            }
        }

    )
});