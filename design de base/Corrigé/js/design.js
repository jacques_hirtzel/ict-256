//$(document).ready(function(){
$(function(){
    //alert("Hello World");

    var page_original = true;

    $("img").on("click",function(){
        if(page_original) {
            $("#content").css("background", "gray");
            $("h1, h2:not('.anecdote')")
                .css("color", "red")
                .css("border-color", "red");
            page_original = false;
        }else{
            $("#content").css("background", "");
            $("h1, h2:not('.anecdote')")
                .css("color", "")
                .css("border-color", "");
            //location.reload();
            page_original = true;
        }
        console.log(page_original);
    });
    

    $("h1").css("width","0");
    $("h1").css("overflow","hidden");

    var texte_titre = $("h1").html();
    texte_titre = texte_titre.replace(" ", "&nbsp;");
    $("h1").html(texte_titre);
    //Ou :
    //$("h1").html($("h1").html().replace(" ", "&nbsp;"));

    $( "h1" ).animate({
        width:400
    }, 2000, function() {
        // Animation complete.
    });

   // $("h1").css("width","400").animate("800");
});